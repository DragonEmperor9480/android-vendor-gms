PRODUCT_PACKAGES += \
    PixelBuiltInPrintService \
    PixelContactsProvider \
    GoogleConfigOverlay \
    GooglePermissionControllerOverlay \
    PixelSettingsGoogle \
    PixelSettingsProvider \
    SystemUIGXOverlay \
    PixelSystemUIGoogle \
    Pixelframework-res \
